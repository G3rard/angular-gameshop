import { Injectable, EventEmitter } from '@angular/core';
import { ApiGames } from '../games/api/api-games.models';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  $openModal = new EventEmitter<boolean>();
  $selectGame = new EventEmitter<ApiGames>();
  $canEdit = new EventEmitter<boolean>();

}
