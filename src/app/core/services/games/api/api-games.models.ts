export interface ApiGames {

    _id?: string,
    name: string,
    platforms: string[],
    price: number,
    year: number,
    genre: string,
    description?: string,
    cover?: string,
    imageDetails1?: string,
    imageDetails2?: string,
    imageDetails3?: string
    

}   