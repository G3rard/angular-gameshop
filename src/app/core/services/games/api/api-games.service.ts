import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Games } from '../games.model';
import { ApiGames } from './api-games.models';
const API_GAMES_URL = "https://gamesapidata.herokuapp.com"


@Injectable({
  providedIn: 'root'
})
export class ApiGamesService {

  constructor(
    private http: HttpClient
  ) { }

public getApiGames(): Observable<ApiGames[]> {

  return this.http.get<ApiGames[]>(`${API_GAMES_URL}/games`)

}

public getApiGamesDetail(_id: string): Observable<ApiGames> {

  return this.http.get<ApiGames>(`${API_GAMES_URL}/games/${_id}`)

}

public postApiNewGame(body: Games): Observable<ApiGames> {

  return this.http.post<ApiGames>(`${API_GAMES_URL}/games`, body)
}


public putApiUpdateGame(_id: string ,body: Games): Observable<ApiGames> {

  return this.http.put<ApiGames>(`${API_GAMES_URL}/games/${_id}`, body)
}

}
