import { Injectable } from '@angular/core';
import { ApiGamesService } from 'src/app/core/services/games/api/api-games.service';
import { map, Observable } from 'rxjs';
import { ApiGames } from './api/api-games.models';
import { Games } from './games.model';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  constructor(private ApiGamesService: ApiGamesService) { }

  public getGames(): Observable<Games[]> {

    return this.ApiGamesService.getApiGames().pipe(
      map((games: ApiGames[]) => {
        return games.map((game) =>  game)
      })
    )
  }

  public getGameDetail(_id:string): Observable<Games> {
    return this.ApiGamesService.getApiGamesDetail(_id).pipe (
      
      map((game) => game)
    )
      
    
  }

  public createGame(body: Games): Observable<Games> {
    return this.ApiGamesService.postApiNewGame(body).pipe(
      map((game) => game)
    )
  }

  public updateGame(_id: string , body: Games): Observable<Games> {
    return this.ApiGamesService.putApiUpdateGame(_id, body).pipe(
      map((game) => game)
    )
  }
}


