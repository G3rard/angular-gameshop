// import { ApiGames } from "./api/api-games.models";

// export const games: ApiGames[] = [
//     {
//         "_id": 1,
//         "name": "Halo Infinite22222",
//         "platforms": ["Xbox One","Microsoft Windows","Xbox Series X|S"],
//         "price": 50,
//         "year": 2021,
//         "genre": "disparos en primera persona",
//         "description": "La legendaria serie de Halo vuelve con la campaña del Jefe Maestro más extensa hasta la fecha y un modo multijugador gratuito totalmente innovador. El jefe Maestro el gran héroe de la humanidad y personaje principal de la saga se enfrentara a un grupo sanguinario y temible que alguna vez fueron parte del Covenant llamados los Desterrados, estos fueron integrados desde Halo Wars 2 y en las novelas del universo de Halo, ahora debutando como la amenaza principal en Halo Infinite.",
//         "cover": "https://store-images.s-microsoft.com/image/apps.21536.13727851868390641.c9cc5f66-aff8-406c-af6b-440838730be0.68796bde-cbf5-4eaa-a299-011417041da6?w=200",
//         "imageDetails1": "https://store-images.s-microsoft.com/image/apps.56154.14330850369313893.72a00f4f-1e43-49df-a85b-55248ac962dc.c1f00aae-e30d-403f-92cc-e2831e88d2e4",
//         "imageDetails2": "",
//         "imageDetails3": "",
//     },
    
//     {
//         "_id": 2,
//         "name": "Grand Theft Auto V",
//         "platforms": ["Xbox One","Microsoft Windows","Xbox Series X|S","Playstation 3","Playstation 4","Playstation 5"],
//         "price": 30,
//         "year": 2013,
//         "genre": "Acción",
//         "description": "Explora el impresionante mundo de Los Santos y el condado de Blaine en la experiencia definitiva de Grand Theft Auto V, con una variedad de actualizaciones técnicas y mejoras para jugadores nuevos y experimentados.  El juego está ambientado en la ciudad ficticia de Los Santos, así como en las zonas que la rodean, basada en la ciudad de Los Ángeles y el sur de California.",
//         "cover": "https://store-images.s-microsoft.com/image/apps.32034.68565266983380288.0f5ef871-88c0-45f7-b108-6aacbc041fcf.9b094362-c51d-49e5-9e92-80710c585fca?w=200",
//         "imageDetails1": "https://store-images.s-microsoft.com/image/apps.52182.68565266983380288.0f5ef871-88c0-45f7-b108-6aacbc041fcf.0226b0f3-27d9-438e-bc44-76921ee9beee",
//         "imageDetails2": "",
//         "imageDetails3": "",
//     },

//     {
//         "_id": 3,
//         "name": "Age of Empires II: Definitive Edition",
//         "platforms": ["Xbox One","Microsoft Windows","Xbox Series X|S"],
//         "price": 60,
//         "year": 2019,
//         "genre": "Estrategia en tiempo real",
//         "description": "Age of Empires II: Definitive Edition es un videojuego de estrategia en tiempo real desarrollado por Forgotten Empires y publicado por Xbox Game Studios . Es una remasterización definitiva del juego original Age of Empires II: The Age of Kings, que celebra el vigésimo aniversario del original.",
//         "cover": "https://store-images.s-microsoft.com/image/apps.55056.13678235101671609.c350aa6a-23e2-407c-94fd-5050e9bedb6f.f8b5d931-11f6-46e3-859f-54981d5b9d1b?w=200",
//         "imageDetails1": "https://store-images.s-microsoft.com/image/apps.29208.13678235101671609.6ca6b3d7-f538-4bc0-aeb7-42f19e88b13b.0a2d119f-b324-453a-87e8-892e58af3cab",
//         "imageDetails2": "",
//         "imageDetails3": "",
//     },

//     {
//         "_id": 4,
//         "name": "The Witcher 3: Wild Hunt  Complete Edition",
//         "platforms": ["Xbox One","Microsoft Windows","Xbox Series X|S","Playstation 3","Playstation 4","Playstation 5"],
//         "price": 50,
//         "year": 2015,
//         "genre": "ARPG",
//         "description": "The Witcher 3: Wild Hunt Es un juego de perspectiva en tercera persona, el jugador controla al protagonista Geralt de Rivia, un cazador de monstruos conocido como Lobo Blanco, es un brujo, el cual emprende un largo viaje a través de Los reinos del norte. El jugador lucha contra el peligroso mundo mediante magia y espadas, mientras interactúa con los NPC y completa tanto misiones secundarias como la misión principal de la historia, La cual es encontrar a la hija adoptiva de Geralt, Ciri, y detener la profecía del invierno blanco ",
//         "cover": "https://store-images.s-microsoft.com/image/apps.53717.65858607118306853.39ed2a08-df0d-4ae1-aee0-c66ffb783a34.80ba72da-abfb-4af6-81f2-a443d12fb870?w=200",
//         "imageDetails1": "https://store-images.s-microsoft.com/image/apps.29313.65858607118306853.0646c27c-9fb5-4af2-a9ee-f673ee1ef239.7514b315-ea01-4acd-8f96-2f0192b9da26",
//         "imageDetails2": "",
//         "imageDetails3": "",
        

//     },

//      ]