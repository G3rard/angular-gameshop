import { Component } from '@angular/core';
import { ModalService } from 'src/app/core/services/modal/modal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public canEdit?: boolean;

  constructor(private modalss: ModalService) {

    this.modalss.$canEdit.subscribe((value) => { this.canEdit = value})

  
  
  }

  offCanEdit(){

    this.canEdit = false

  }

}
