import { Pipe, PipeTransform } from '@angular/core';
import { ApiGames } from 'src/app/core/services/games/api/api-games.models';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform( value: ApiGames[], arg: string): ApiGames[] {

    if(arg == '' || arg.length < 3) {return value;}
    const res = [];
    for(const games of value){
      if(games.name.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        res.push(games);
      };


    };

    return res;
 
  }

}
