import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ApiGames } from 'src/app/core/services/games/api/api-games.models';
import { ModalService } from 'src/app/core/services/modal/modal.service';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent {


  @Input() public games?: ApiGames;
  @Output() public selectedGame = new EventEmitter<ApiGames>()

  public openModal?: boolean = true;







  constructor(private modalss: ModalService) { }

  openModalDetail = (saveGame: ApiGames) => {

    this.modalss.$openModal.emit(this.openModal);
    this.modalss.$selectGame.emit(saveGame);
    this.selectedGame.emit(saveGame);



  }



}



