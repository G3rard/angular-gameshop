import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalService } from 'src/app/core/services/modal/modal.service';
import { ApiGames } from 'src/app/core/services/games/api/api-games.models';
import { Router } from '@angular/router';


@Component({
  selector: 'app-modal-detail',
  templateUrl: './modal-detail.component.html',
  styleUrls: ['./modal-detail.component.scss']
})
export class ModalDetailComponent {

  public gamesSelect?: ApiGames

  @Output() public closeModal = new EventEmitter<boolean>();

  constructor(private router: Router, private modalss: ModalService) {

    this.modalss.$selectGame.subscribe((value) => { this.gamesSelect = value })

  }

  // 

  public editGame() {
    this.router.navigate(['new-game'], { queryParams: { _id: this.gamesSelect?._id } })
  }

  public detailButton() {
    if (this.gamesSelect) {
      this.router.navigate(['detail', this.gamesSelect._id])
    };

  }


  closeModalDetail() {
    this.closeModal.emit(false)
    console.log(this.gamesSelect?._id)
    console.log(this.gamesSelect)

  }
}







