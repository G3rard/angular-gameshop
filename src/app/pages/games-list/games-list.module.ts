import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamesListComponent } from './games-list.component';
import { GamesComponent } from './components/games/games.component';
import { GameListRoutingModule } from './games-list-routing,module';
import { RouterModule } from '@angular/router';
import { ModalDetailComponent } from './components/modal-detail/modal-detail.component';
import { FilterPipe } from './filter/filter.pipe';
import { FormsModule } from '@angular/forms';





@NgModule({
  declarations: [
    GamesListComponent,
    GamesComponent,
    ModalDetailComponent,
    FilterPipe,
  ],
  imports: [
    CommonModule,
    GameListRoutingModule,
    RouterModule,
    FormsModule
  ],
})
export class GamesListModule { }
