import { Component, OnInit } from '@angular/core';
import { ApiGamesService } from 'src/app/core/services/games/api/api-games.service';
import { ModalService } from 'src/app/core/services/modal/modal.service';
import { ApiGames } from './../../core/services/games/api/api-games.models';


@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss']
})
export class GamesListComponent implements OnInit {

public filterGame: string = '';

public games: ApiGames[] = []

constructor(private modalss: ModalService, private gamesService:ApiGamesService) {

  this.modalss.$openModal.subscribe((value) => { this.isActiveDetail = value})


}

public ngOnInit(): void {

  this.gamesService.getApiGames().subscribe((res) => {this.games = res })

  
  
}

public isActiveDetail?: boolean 



off(event: boolean) {
  this.isActiveDetail = event
}



}







