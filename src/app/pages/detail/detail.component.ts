import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiGamesService } from 'src/app/core/services/games/api/api-games.service';
import { Games } from './../../core/services/games/games.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent{

  public game?: Games;

  constructor(private activatedRoute: ActivatedRoute, private gamesService:ApiGamesService) {
    
    

    
    this.activatedRoute.params.subscribe((params) => {

     const gameId = params['id'];

     this.gamesService.getApiGamesDetail(gameId).subscribe((value) => {
      this.game = value
     })

     


    }

    )

  }


  
}
