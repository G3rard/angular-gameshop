import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewGameRoutingModule } from './new-game-routing.module';
import { NewGameComponent } from './new-game.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    NewGameComponent
  ],
  imports: [

  CommonModule,
    NewGameRoutingModule,
    ReactiveFormsModule,
    RouterModule,
    
  ]
})
export class NewGameModule { }
