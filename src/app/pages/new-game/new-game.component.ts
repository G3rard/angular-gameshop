import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { map, of, switchMap } from 'rxjs';
import { Games } from 'src/app/core/services/games/games.model';
import { GamesService } from 'src/app/core/services/games/games.service';
import { ModalService } from 'src/app/core/services/modal/modal.service';



@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent {

  public gameForm?: FormGroup;
  public canEdit?: boolean = false;
  public gameId?: string;

  constructor(
    private gameBuilder: FormBuilder, 
    private activateRoute: ActivatedRoute, 
    private gameService: GamesService, 
    private modalss: ModalService,
    private router: Router
    ) 
  
  {
    
    this.activateRoute.queryParams.pipe(
      map((queryParams) => queryParams["_id"]),
      switchMap((_id: string) => {
        if(!_id){

          return of(undefined)

        }else{
          this.gameId = _id
          return this.gameService.getGameDetail(_id)


        }
      })

    ).subscribe((game?: Games) => {

      this.canEdit = !!game;

      this.createNewForm(game);

      this.modalss.$canEdit.emit(this.canEdit);

    })
  
     


  

  }


  public createNewForm(game?: Games){
   
   
    this.gameForm = this.gameBuilder.group({

      name: new FormControl(game?.name || '', [Validators.required]),
      platforms: new FormControl(game?.platforms || '', [Validators.required]),
      price: new FormControl(game?.price || '', [Validators.required, Validators.maxLength(5)]),
      year: new FormControl(game?.year ||'', [Validators.required, Validators.maxLength(4)]),
      genre: new FormControl(game?.genre || '', [Validators.required]),
      description: new FormControl(game?.description ||'', [Validators.required]),
      cover: new FormControl(game?.cover ||''),
      imageDetails1: new FormControl(game?.imageDetails1 ||''),




    })
  }


  public createNewGame() {
// if(!this.gameForm?.valid) {return}
    const gameRequest = this.canEdit && this.gameId ? this.gameService.updateGame(
      this.gameId, this.gameForm?.value
    ) : this.gameService.createGame(this.gameForm?.value);

    gameRequest.subscribe(() => {
      this.router.navigate(['games'])
      this.gameForm?.reset()
    })
}



}
