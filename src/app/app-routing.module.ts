import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },


  {

    path: 'home',

    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
    
    },


{
  path: 'games',
  loadChildren: () => import('./pages/games-list/games-list.module').then(m => m.GamesListModule)
},

{
  path: 'detail/:id',
  loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailModule)
},

{
  path: 'new-game',
  loadChildren: () => import('./pages/new-game/new-game.module').then(m => m.NewGameModule)
},

{
  path: 'create-product',
  loadChildren: () => import('./pages/new-game/new-game.module').then(m => m.NewGameModule),
},


{
  path: '**',
  loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule)
  
},


];

@NgModule({ 
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
